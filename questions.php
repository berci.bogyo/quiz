<?php

$questions = array (
array(
    'number'=>'1.',
    'question' => 'Which command show your working directory?',
    'name'=> 'linux-commands',
    'answer1' => 'cd',
    'answer2' => 'ls',
    'answer3' => 'pwd',
    'answer4' => 'mv'
),
array(
    'number'=>'2.',
    'question' => 'How to create a directory in Linux?',
    'name'=> 'mkdir',
    'answer1' => 'mkdir',
    'answer2' => 'create dir',
    'answer3' => 'make directory',
    'answer4' => 'touch'
),
array (
    'number'=>'3.',
    'question' => 'How to make an apache server?',
    'name'=> 'apache',
    'answer1' => 'there is no command',
    'answer2' => 'what? ',
    'answer3' => 'sudo service apache2 start',
    'answer4' => 'apache2 start sudo service'
),
array (
    'number'=>'4.',
    'question' => 'How do you initialize git repository on your machine?',
    'name'=> 'gitrepo',
    'answer1' => 'git status',
    'answer2' => 'git init',
    'answer3' => 'git add',
    'answer4' => 'git commit'
),
array (
    'number'=>'5.',
    'question' => 'How do you give every permission for everybody in the "test" directory?',
    'name'=> 'permission',
    'answer1' => 'sudo chmod 777 test',
    'answer2' => 'sudo chmod 711 test',
    'answer3' => 'sudo chmod 111 test',
    'answer4' => 'sudo chmod 777'
),
array (
    'number'=>'6.',
    'question' => 'How do you change branch on git?',
    'name'=> 'gitbranch',
    'answer1' => 'git change branch branch-name',
    'answer2' => 'git branch branch-name',
    'answer3' => 'git checkout branch-name',
    'answer4' => 'git create branch'
),
array (
    'number'=>'7.',
    'question' => 'How to add all files to the staging area in git??',
    'name'=> 'gitstage',
    'answer1' => 'git add',
    'answer2' => 'git add .',
    'answer3' => 'git stage',
    'answer4' => 'git commit'
),
array (
    'number'=>'8.',
    'question' => 'Start mysql server! Which command will you use?',
    'name'=> 'mysql',
    'answer1' => 'sudo service mysql server',
    'answer2' => 'sudo service server mysql',
    'answer3' => 'sudo service mysql start',
    'answer4' => 'sudo mysql start'
),
array (
    'number'=>'9.',
    'question' => 'How can you edit the "test.txt" file in terminal?',
    'name'=> 'nano',
    'answer1' => 'nano meter',
    'answer2' => 'edit file',
    'answer3' => 'nano test.txt',
    'answer4' => 'edit nano test.txt'
),
array (
    'number'=>'10.',
    'question' => 'How do you change the FILE\'s ownership?',
    'name'=> 'owner',
    'answer1' => 'chown FILE',
    'answer2' => 'chown USER',
    'answer3' => 'chown FILE USER',
    'answer4' => 'chown USER FILE'
)

);
$good_answers = array(
    'pwd','mkdir', 
    'sudo service apache2 start', 
    'git init', 
    'sudo chmod 777 test', 
    'git checkout branch-name', 
    'git add .', 
    'sudo service mysql start', 
    'nano test.txt', 
    'chown USER FILE'
);


