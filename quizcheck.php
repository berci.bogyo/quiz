<?php
include("header.php");
include("questions.php");
?>
<div class="result-container">
<?php
$points = 0;
$user_answers = [];


if(ISSET($_POST['submit'])){
    array_push($user_answers, 
        $_POST['linux-commands'], 
        $_POST['mkdir'], 
        $_POST['apache'], 
        $_POST['gitrepo'], 
        $_POST['permission'], 
        $_POST['gitbranch'], 
        $_POST['gitstage'], 
        $_POST['mysql'], 
        $_POST['nano'], 
        $_POST['owner']);

   for(
       $i = 0;
       $i < count($good_answers); 
       $i ++){

            if($good_answers[$i] === $user_answers[$i]){
                $points ++;
            } else {
                echo "<h3>The proper answer for the ".($i+1). ". question was: ". $good_answers[$i]."</h3>";
            }
        }
}
echo "<h2>You have earned : ". $points ." point".(($points>2) ? "s" : "")."</h2>";

?>
</div>
<?php
include("footer.php");
?>