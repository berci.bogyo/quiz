# QUIZ
Newly created quiz for the first practice section.
## Install 
Clone or download the files
## Create an apache server
```bash
sudo service apache2 start
```
## Set up
Moved the cloned files to your server's folder
```bash
mv test ../../../var/www/html
```
## Open in the browser, type the following URL:
```bash
localhost/quiz
```


